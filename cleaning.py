class Cars:
  def clean_cars():
    print("** Cleaning cars **")

    rows = H.get_file_rows("cars.csv")
    make_prices = [[H.get_nth_item(row, 0), H.get_last_item(row)] for row in rows[1:]]
    make_averages = Cars.count_averages(make_prices)
    rows = Cars.fix_car_nulls(rows, make_averages)
    print("** Clean complete **")

    H.save_file(rows, "cars.csv")

  def fix_car_nulls(make_prices, make_averages):
    rows = [make_prices[0]]
    for item in make_prices[1:]:
      rows.append(item.replace('NULL', "%.0f" % make_averages[H.get_nth_item(item, 0)]))

    return rows

  def count_averages(make_prices):
    make_prices_counts = dict()
    make_prices_prices = dict()
    make_averages = dict()

    for make,price in make_prices:
      if(price != 'NULL'):
        H.add_or_update(make_prices_counts, make, 1)
        H.add_or_update(make_prices_prices, make, int(price))

    for key in make_prices_counts:
      make_averages[key] = make_prices_prices[key]/make_prices_counts[key]

    return make_averages

class H:
  def get_file_rows(filename):
    file = open("input/" + filename, "r")
    return file.readlines()

  def save_file(rows, filename):
    file = open("cleaned/" + filename, "w")
    for row in rows:
      file.write(row)
    print("** File " + filename + " saved **")

  def get_second_item(row):
    return H.get_nth_item(row, 1)

  def get_nth_item(row, n):
    return row.split(";")[n]

  def get_last_item(row):
    return row.split(";")[-1].replace("\n", "")

  def add_or_update(dictionary, key, value):
    if key in dictionary:
      dictionary[key] += value
    else:
      dictionary[key] = value

  def distinct(iterable, keyfunc=None):
      seen = set()
      for item in iterable:
          key = item if keyfunc is None else keyfunc(item)
          if key not in seen:
              seen.add(key)
              yield item

class Engines:
  def clean_engines(filename):
    print("** Cleaning " +filename+" **")
    engines = H.get_file_rows(filename)
    engines_distinct = list(H.distinct(engines, H.get_second_item))
    print("** Clean complete **")
    H.save_file(engines_distinct, filename)

Cars.clean_cars()
Engines.clean_engines("gas_engine.csv")
Engines.clean_engines("diesel_engine.csv")
